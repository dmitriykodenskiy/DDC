  ------------------
  Project Structure
  ------------------
  The project is made using preprocessors pug(for html) and stylus(for css).
  
  WHAT IS WHAT?
  
  In /src/assets/ you can find all the images, fonts, some basic styles and js with initialization of vendor libraries.
  
  In /src/blocks/ you will find pug, stylus and js files for all blocks of the project. Nessesary blocks are included into appropriate pages.
  
  In /src/pages/ you will find all the pages of the project, which include nessesary blocks. You can include/exclude/change order of any block in the page. 
  
  In /src/tabs/ you will find pug files of the tabs.
  
  In /src/layouts/ you can find the basic pug file, which is extended by all other pages.
  
  In /src/globals/ you will find all the content(text, links etc.) of the project and file with stylus variables and mixins used in the project.
  
  
  
  ------------ 
  Installation
  ------------
  $ npm i
  
  
  
  ------------- 
  Documentation
  -------------

  Start development
  
  $ gulp
  
  Build production bundle
  
  $ gulp build



 