/* gallery */
var gallerySlideNow = 1;
var gallerySlideCount = $(".gallery-viewport__slider").children().length;
var galleryTranslateWidth = 0;


function galleryNextSlide() {
    if (gallerySlideNow == gallerySlideCount || gallerySlideNow <= 0 || gallerySlideNow > gallerySlideCount) {
        $('.gallery-viewport__slider').css('transform', 'translate(0, 0)');
        gallerySlideNow = 1;
    } else {
        galleryTranslateWidth = -$('.gallery-viewport__slider-block').width() * (gallerySlideNow);
        $('.gallery-viewport__slider').css({
            'transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
        });
        gallerySlideNow++;
    }
}
function galleryPrevSlide() {
    if (gallerySlideNow == 1 || gallerySlideNow <= 0 || gallerySlideNow > gallerySlideCount) {
        galleryTranslateWidth = -$('.gallery-viewport__slider-block').width() * (gallerySlideCount - 1);
        $('.gallery-viewport__slider').css({
            'transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
        });
        gallerySlideNow = gallerySlideCount;
    } else {
        galleryTranslateWidth = -$('.gallery-viewport__slider-block').width() * (gallerySlideNow - 2);
        $('.gallery-viewport__slider').css({
            'transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + galleryTranslateWidth + 'px, 0)',
        });
        gallerySlideNow--;
    }
}

$('.gallery-viewport__arrow_right').click(function() {
        galleryNextSlide();
    });
$('.gallery-viewport__arrow_left').click(function() {
        galleryPrevSlide();
});
$('.gallery-item').click(function(){
    if ($(this).hasClass("gallery-item_active")){
        $(".gallery-viewport").css({'visibility':'visible'});
        $(".gallery-viewport__arrow_left").css({'display':'block'});
        $(".gallery-viewport__arrow_right").css({'display':'block'});
        $(".gallery-viewport__close").css({'display':'block'});
    }
    else {
        $(".gallery-item_active").removeClass("gallery-item_active");
        $(this).addClass("gallery-item_active");
    }
});

$(".gallery-viewport__close").click(function(){
    $(".gallery-viewport").css({'visibility':'collapse'});
    $(".gallery-viewport__arrow_left").css({'display':'none'});
    $(".gallery-viewport__arrow_right").css({'display':'none'});
    $(".gallery-viewport__close").css({'display':'none'});
})