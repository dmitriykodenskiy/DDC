$(document).ready(function() {
    
            /* category tabs */
    
          $('.tabs__list-link_1').click(function() {
              $('.tabs-container').load('../../tabs/tab1.html');
          });
          $('.tabs__list-link_2').click(function() {
              $('.tabs-container').load('../../tabs/tab2.html');
          });
          $('.tabs__list-link_3').click(function() {
              $('.tabs-container').load('../../tabs/tab3.html');
          });
    
            /* lazers tabs */
    
          $('.lazer1__list-item_link1').click(function() {
              $('.lazer1__text-block_4').load('../../tabs/lazer1_tab1.html');
          });
          $('.lazer1__list-item_link2').click(function() {
              $('.lazer1__text-block_4').load('../../tabs/lazer1_tab2.html');
          });
          $('.lazer1__list-item_link3').click(function() {
              $('.lazer1__text-block_4').load('../../tabs/lazer1_tab3.html');
          });
    
          
    
          $('.lazer2__list-item_link1').click(function() {
              $('.lazer2__text-block_4').load('../../tabs/lazer2_tab1.html');
          });
          $('.lazer2__list-item_link2').click(function() {
              $('.lazer2__text-block_4').load('../../tabs/lazer2_tab2.html');
          });
          $('.lazer2__list-item_link3').click(function() {
              $('.lazer2__text-block_4').load('../../tabs/lazer2_tab3.html');
          });
    
    
          $('.lazer3__list-item_link1').click(function() {
              $('.lazer3__text-block_4').load('../../tabs/lazer3_tab1.html');
          });
          $('.lazer3__list-item_link2').click(function() {
              $('.lazer3__text-block_4').load('../../tabs/lazer3_tab2.html');
          });
          $('.lazer3__list-item_link3').click(function() {
              $('.lazer3__text-block_4').load('../../tabs/lazer3_tab3.html');
          });
    
            /* optic tabs */
    
          $('.lazerOptics__list-item_item1').click(function() {
              $('.lazerOptics__text-block_4').load('../../tabs/lazerOptics_tab1.html');
          });
          $('.lazerOptics__list-item_item2').click(function() {
              $('.lazerOptics__text-block_4').load('../../tabs/lazerOptics_tab2.html');
          });
          $('.lazerOptics__list-item_item3').click(function() {
              $('.lazerOptics__text-block_4').load('../../tabs/lazerOptics_tab3.html');
          });
          $('.lazerOptics__list-item_item4').click(function() {
              $('.lazerOptics__text-block_4').load('../../tabs/lazerOptics_tab4.html');
          });
    
          
    
          $('.stringray__list-item_link1').click(function() {
              $('.stringray__text-block_4').load('../../tabs/stringray_tab1.html');
          });
          $('.stringray__list-item_link2').click(function() {
              $('.stringray__text-block_4').load('../../tabs/stringray_tab2.html');
          });
          $('.stringray__list-item_link3').click(function() {
              $('.stringray__text-block_4').load('../../tabs/stringray_tab3.html');
          });
          $('.stringray__list-item_link4').click(function() {
              $('.stringray__text-block_4').load('../../tabs/stringray_tab4.html');
          });
    
    
            /* FAQ tabs */
    
          $('.faq__question_1').click(function() {
              $('.faq__section_2').load('../../tabs/answer1.html');
          });
          $('.faq__question_2').click(function() {
              $('.faq__section_2').load('../../tabs/answer2.html');
          });
          $('.faq__question_3').click(function() {
              $('.faq__section_2').load('../../tabs/answer3.html');
          });
          $('.faq__question_4').click(function() {
              $('.faq__section_2').load('../../tabs/answer4.html');
          });
          $('.faq__question_5').click(function() {
              $('.faq__section_4').load('../../tabs/answer5.html');
          });
          $('.faq__question_6').click(function() {
              $('.faq__section_4').load('../../tabs/answer6.html');
          });
          $('.faq__question_7').click(function() {
              $('.faq__section_4').load('../../tabs/answer7.html');
          });
          $('.faq__question_8').click(function() {
              $('.faq__section_4').load('../../tabs/answer8.html');
          });
      });