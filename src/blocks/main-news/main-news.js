var slideNow = 1;
var slideCount = $(".viewport__slider").children().length;
var translateWidth = 0;


function nextSlide() {
    if (slideNow == slideCount || slideNow <= 0 || slideNow > slideCount) {
        $('.viewport__slider').css('transform', 'translate(0, 0)');
        $('.main-testimonials__inner_content-wrapper').css('transform', 'translate(0, 0)');
        slideNow = 1;
    } else {
        translateWidth = -$('.viewport__slider-block').width() * (slideNow);
        $('.viewport__slider').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        translateTestimonialsWidth = -$('.main-testimonials').width() * (slideNow);
        $('.main-testimonials__inner_content-wrapper').css({
            'transform': 'translate(' + translateTestimonialsWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateTestimonialsWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateTestimonialsWidth + 'px, 0)',
        });
        slideNow++;
    }
}
function prevSlide() {
    if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
        translateWidth = -$('.viewport__slider-block').width() * (slideCount - 1);
        $('.viewport__slider').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        translateTestimonialsWidth = -$('.main-testimonials').width() * (slideCount - 1);
        $('.main-testimonials__inner_content-wrapper').css({
            'transform': 'translate(' + translateTestimonialsWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateTestimonialsWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateTestimonialsWidth + 'px, 0)',
        });
        slideNow = slideCount;
    } else {
        translateWidth = -$('.viewport__slider-block').width() * (slideNow - 2);
        $('.viewport__slider').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        translateWidth = -$('.main-testimonials').width() * (slideNow - 2);
        $('.main-testimonials__inner_content-wrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow--;
    }
}

$('.main-testimonials__arrow_left').click(function() {
        nextSlide();
    });
$('.viewport__arrow_left').click(function() {
        nextSlide();
    });
$('.main-testimonials__arrow_right').click(function() {
        prevSlide();
});
$('.viewport__arrow_right').click(function() {
        prevSlide();
});
