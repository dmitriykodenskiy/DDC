$(document).ready(function() {
	$('#fullpage').fullpage({
		//options here
		autoScrolling:true,
        normalScrollElements: '.main-seo__text, .aboutPageAbout__text_full, .news-full__text_full, .contact__inner_4, .faq__inner_2, .faq__inner_4, .lazer1__text-block_4, .lazer2__text-block_4, .lazer3__text-block_4, .optichamber__text-block_4, .optichamber2__text-block_4, .laskin__text-block_4, .downloadsPage__inner',
        lazyLoading: true
	});

	//methods
	$.fn.fullpage.setAllowScrolling(true);
});